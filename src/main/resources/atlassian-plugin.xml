<atlassian-plugin key="${atlassian.plugin.key}"
    name="${project.name}" pluginsVersion="2">
    <plugin-info>
        <description>${atlassian-plugin.description}</description>
        <version>${atlassian-plugin.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}"/>
        <param name="atlassian-data-center-compatible">true</param>
    </plugin-info>

    <customfield-type key="participants" name="Participants of an issue"
        class="com.atlassian.jira.toolkit.customfield.ParticipantsCFType">
        <description>Displays reporter, current assignee and all commenters of the issue</description>
        <resource type="velocity" name="view" location="templates/view-participants.vm"/>
        <resource type="velocity" name="column-view" location="templates/col-participants.vm"/>
        <resource type="velocity" name="xml" location="templates/xml/xml-participants.vm"/>
    </customfield-type>

    <customfield-type key="lastusercommented" name="Last commented by a User Flag"
        class="com.atlassian.jira.toolkit.customfield.LastCommentedByUserCFType">
        <description>Displays true if last commenter who is not a JIRA developer (jira-developers group member)</description>
        <resource type="velocity" name="view" location="templates/plugins/fields/view/view-basictext.vm"/>
        <resource type="velocity" name="column-view" location="templates/plugins/fields/view/view-basictext.vm"/>
        <resource type="velocity" name="xml" location="templates/xml/xml-basictext.vm"/>
    </customfield-type>

    <customfield-type key="dayslastcommented" name="Days since last comment"
        class="com.atlassian.jira.toolkit.customfield.DaysLastCommented">
        <description>Date/time since last comment (restricted comments are considered on a per user basis). This field is NOT searchable or sortable.</description>
        <resource type="velocity" name="view" location="templates/last-commented.vm"/>
        <resource type="velocity" name="xml" location="templates/xml/xml-lastcommented.vm"/>
    </customfield-type>

    <customfield-type key="LastCommentDate" name="Last public comment date"
                      class="com.atlassian.jira.toolkit.customfield.LastCommentDate">
        <description>Date/time of last public comment (restricted comments are ignored). This field is searchable and sortable.</description>
        <resource type="velocity" name="view" location="templates/last-comment-date.vm"/>
        <resource type="velocity" name="xml" location="templates/xml/xml-last-comment-date.vm"/>
    </customfield-type>

    <customfield-type key="reporterdomain" name="Domain of Reporter"
        class="com.atlassian.jira.toolkit.customfield.EmailDomainNameCFType">
        <description>The domain name of the reporter</description>
        <resource type="velocity" name="view" location="templates/view-domain.vm"/>
        <resource type="velocity" name="column-view" location="templates/col-domain.vm"/>
	    <resource type="velocity" name="xml" location="templates/xml/xml-basictext.vm"/>
    <param name="role" value="reporter"/>
    </customfield-type>

    <customfield-type key="assigneedomain" name="Domain of Assignee"
	class="com.atlassian.jira.toolkit.customfield.EmailDomainNameCFType">
	<description>The domain name of the assignee</description>
	<resource type="velocity" name="view" location="templates/view-domain.vm"/>
	<resource type="velocity" name="column-view" location="templates/col-domain.vm"/>
    <resource type="velocity" name="xml" location="templates/xml/xml-basictext.vm"/>
    <param name="role" value="assignee"/>
    </customfield-type>

    <customfield-type key="supporttools" name="Support Tool Bar"
        class="com.atlassian.jira.toolkit.customfield.NullCFType">
        <description>Displays the some action links on the issue navigator (Obsolete)</description>
        <resource type="velocity" name="column-view" location="templates/support-tools.vm"/>
    </customfield-type>

  <customfield-type key="message" name="Message Custom Field (for edit)"
                    class="com.atlassian.jira.toolkit.customfield.DefaultTextCFType">
    <description>A custom field whose default value will be displayed as HTML</description>
    <resource type="velocity" name="edit" location="templates/message.vm"/>
  </customfield-type>

  <customfield-type key="viewmessage" name="Message Custom Field (for view)"
                    class="com.atlassian.jira.toolkit.customfield.DefaultTextCFType">
    <description>A custom field whose default value will be displayed as HTML</description>
    <resource type="velocity" name="edit" location="templates/edit-message-for-view.vm"/>
    <resource type="velocity" name="view" location="templates/view-message-for-view.vm"/>
  </customfield-type>

    <customfield-type key="multikeyfield" name="Multi Issue Key Searcher"
        class="com.atlassian.jira.toolkit.customfield.MultiIssueKeyCFType">
        <description>Search for and order multiple issue by Issue Key</description>
        <resource type="velocity" name="column-view" location="templates/col-multikey.vm"/>
    </customfield-type>

    <customfield-searcher key="multikeysearcher" name="Multi Issue Searcher"
        class="com.atlassian.jira.toolkit.customfield.MultiIssueKeySearcher">
        <description>Search for and order multiple issue by Issue Key</description>

        <resource type="velocity" name="search" location="templates/search-multikey.vm"/>
        <resource type="velocity" name="view" location="templates/plugins/fields/view-searcher/view-searcher-basictext.vm"/>
        <valid-customfield-type package="com.atlassian.jira.toolkit" key="multikeyfield"/>
    </customfield-searcher>

    <customfield-searcher key="lastcommentersearcher" name="Last commenter Searcher"
        class="com.atlassian.jira.toolkit.customfield.searchers.LastCommentSearcher">
        <description>Search for true only</description>
        <resource type="velocity" name="search" location="templates/last-commenter.vm"/>
        <resource type="velocity" name="view" location="templates/plugins/fields/view-searcher/view-searcher-basictext.vm"/>

        <valid-customfield-type package="com.atlassian.jira.toolkit" key="lastusercommented"/>
    </customfield-searcher>

    <customfield-searcher key="textsearcher" name="Free Text Searcher"
        class="com.atlassian.jira.toolkit.customfield.searchers.DomainSearcher">
        <description>Search for values using a free text search.</description>

        <resource type="velocity" name="search" location="templates/plugins/fields/edit/edit-basictext.vm"/>
        <resource type="velocity" name="view" location="templates/plugins/fields/view-searcher/view-searcher-basictext.vm"/>
        <resource type="velocity" name="label" location="templates/plugins/fields/view-searcher/label-searcher-basictext.vm"/>

        <valid-customfield-type package="com.atlassian.jira.toolkit" key="reporterdomain"/>
        <valid-customfield-type package="com.atlassian.jira.toolkit" key="assigneedomain"/>
    </customfield-searcher>

    <customfield-searcher key="userpickersearcher" name="User Picker Searcher"
                          class="com.atlassian.jira.toolkit.customfield.searchers.UserPickerSearcher">
        <description>Allow to search for a user using a userpicker.</description>
        <resource type="velocity" name="label"
                  location="templates/plugins/fields/view-searcher/label-searcher-user.vm"/>
        <resource type="velocity" name="search" location="templates/plugins/fields/edit-searcher/search-userpicker.vm"/>
        <resource type="velocity" name="view"
                  location="templates/plugins/fields/view-searcher/view-searcher-basictext.vm"/>
        <valid-customfield-type package="com.atlassian.jira.toolkit" key="participants"/>
        <valid-customfield-type package="com.atlassian.jira.toolkit" key="lastupdaterorcommenter"/>
    </customfield-searcher>

    <customfield-type key="attachments" name="Number of attachments"
                      class="com.atlassian.jira.toolkit.customfield.AttachmentCountCFType">
        <description>A custom field that stores the number of attachments for an issue.</description>

        <resource type="velocity" name="view" location="templates/plugins/fields/view/view-number.vm"/>
        <resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-number.vm"/>
    </customfield-type>

    <customfield-type key="comments" name="Number of comments"
                      class="com.atlassian.jira.toolkit.customfield.CommentCountCFType">
        <description>A custom field that stores the number of comments for an issue.</description>

        <resource type="velocity" name="view" location="templates/plugins/fields/view/view-number.vm"/>
        <resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-number.vm"/>
    </customfield-type>

    <customfield-type key="originalestimate" name="Original Estimate"
                      class="com.atlassian.jira.toolkit.customfield.OriginalEstimateCFType">
        <description>A field that enables original estimate searches by hours and allows in-line editing</description>
    </customfield-type>

    <customfield-searcher key="numberrange" name="Number range searcher"
                          class="com.atlassian.jira.toolkit.customfield.searchers.NumberRangeSearcher">
        <description>Allow searching for a number that is in a given range</description>

        <resource type="velocity" name="search"
                  location="templates/plugins/fields/edit-searcher/search-number-range.vm"/>
        <resource type="velocity" name="view"
                  location="templates/plugins/fields/view-searcher/view-searcher-number-range.vm"/>

        <valid-customfield-type package="com.atlassian.jira.toolkit" key="attachments"/>
        <valid-customfield-type package="com.atlassian.jira.toolkit" key="comments"/>
        <valid-customfield-type package="com.atlassian.jira.toolkit" key="originalestimate"/>
    </customfield-searcher>

    <customfield-searcher key="LastCommentDateSearcher" name="Last comment date time searcher"
                          class="com.atlassian.jira.toolkit.customfield.searchers.LastCommentDateSearcher">
        <description>Allow searching for last public comment date</description>

        <valid-customfield-type package="com.atlassian.jira.toolkit" key="LastCommentDate"/>
    </customfield-searcher>

    <customfield-type key="userproperty" name="User Property Field (&lt; 255 characters)"
                      class="com.atlassian.jira.toolkit.customfield.UserPropertyCFType">
        <description key="admin.customfield.type.userproperty.desc">Allows the display of a user (assignee or reporter)
            property on the view issue page, by using the default value. eg. 'assignee:phonenumber'
        </description>
        <resource type="velocity" name="view" location="templates/view-userproperty.vm"/>
        <resource type="velocity" name="edit-default" location="templates/edit-userproperty.vm"/>
        <resource type="velocity" name="xml" location="templates/xml/xml-userproperty.vm"/>
    </customfield-type>

	<customfield-type key="lastupdaterorcommenter" name="Username of last updater or commenter"
					  class="com.atlassian.jira.toolkit.customfield.AuthorOfLastUpdateOrCommentCFType">
		<description>Display the username of either the last updater or commenter depending on which happened most recently.</description>
        <resource type="velocity" name="view" location="templates/plugins/fields/view/view-user.vm"/>
		<resource type="velocity" name="column-view" location="templates/plugins/fields/view/view-user.vm"/>
		<resource type="velocity" name="xml" location="templates/plugins/fields/xml/xml-user.vm"/>
	</customfield-type>

    <rest name="Gadget REST Endpoint" key="toolkit-rest-points" path="/tkgadgets" version="1.0" description="Provides REST endpoints used to display JIRA Toolkit gadgets." />

    <resource type="i18n" name="i18n" location="com/atlassian/jira/toolkit/i18n" />

    <workflow-condition key="compare-number-condition" name="Compare Number Custom Field" class="com.atlassian.jira.toolkit.workflow.WorkflowCompareNumberCFConditionFactoryImpl">
        <description>Condition to allow transition if a comparison of specified Number Custom Field to a specified value is true.</description>

        <condition-class>com.atlassian.jira.toolkit.workflow.CompareNumberCFCondition</condition-class>

        <resource type="velocity" name="view" location="templates/compare-numbercf-condition-view.vm"/>
        <resource type="velocity" name="input-parameters" location="templates/compare-numbercf-condition-input-params.vm"/>
        <resource type="velocity" name="edit-parameters" location="templates/compare-numbercf-condition-edit-params.vm"/>
    </workflow-condition>
</atlassian-plugin>
